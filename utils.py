#!/usr/bin/env python
# !/usr/local/bin/python

import os


def create_model_dir(model_dir):
    full_dir = "./trained_models/" + model_dir
    if not os.path.exists(full_dir):
        os.makedirs(full_dir + "/0")
        return full_dir + "/0"
    else:
        i = 0
        while os.path.exists(full_dir + "/" + str(i)):
            i += 1
        os.makedirs(full_dir + "/" + str(i))
        return full_dir + "/" + str(i)
